﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestUser.DTO
{
    public class Article
    {
        private decimal price;
        private int id;
        private String name;
        private int quantity;

        public decimal Price { get => price; set => price = value; }
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Quantity { get => quantity; set => quantity = value; }
    }
}
