﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAnnouncement.DTO
{
    public class User
    {
        private String password;
        private String role;
        private String username;
        private String changer;

        public string Role { get => role; set => role = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Changer { get => changer; set => changer = value; }
    }
}
