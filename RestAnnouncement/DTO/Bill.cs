﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestUser.DTO
{
    public class Bill
    {
        private int id;
        private String username;
        private decimal totalPrice;
        private DateTime dateTime;
        private Article[] articles;

        public int Id { get => id; set => id = value; }
        public string Username { get => username; set => username = value; }
        public decimal TotalPrice { get => totalPrice; set => totalPrice = value; }
        public DateTime DateTime { get => dateTime; set => dateTime = value; }
        public Article[] Articles { get => articles; set => articles = value; }
    }
}
