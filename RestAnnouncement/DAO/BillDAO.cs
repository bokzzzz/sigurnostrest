﻿using FootAppREST.Connection;
using MySql.Data.MySqlClient;
using RestUser.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestUser.DAO
{
    public class BillDAO
    {
        public bool AddBill(Bill bill)
        {
                DatabaseConnection db = DatabaseConnection.GetConnection();
                MySqlCommand mySqlCommand = db.CreateCommand();
                mySqlCommand.CommandText = "INSERT into bill(TotalPrice,Username) values(@total,@username);";
                mySqlCommand.Parameters.AddWithValue("@total", bill.TotalPrice);
                mySqlCommand.Parameters.AddWithValue("@username", bill.Username);
                mySqlCommand.Prepare();
                if (mySqlCommand.ExecuteNonQuery() > 0)
                {
                     bill.Id =(int) mySqlCommand.LastInsertedId;
                    foreach(Article article in bill.Articles) {
                    mySqlCommand = db.CreateCommand();
                    mySqlCommand.CommandText = "INSERT into items(IdBill,IdArticle,Price,Quantity) values(@bill,@article,@price,@quantity);";
                    mySqlCommand.Parameters.AddWithValue("@bill", bill.Id);
                    mySqlCommand.Parameters.AddWithValue("@article", article.Id);
                    mySqlCommand.Parameters.AddWithValue("@price", article.Price/article.Quantity);
                    mySqlCommand.Parameters.AddWithValue("@quantity", article.Quantity);
                    mySqlCommand.Prepare();
                    if(mySqlCommand.ExecuteNonQuery() < 1)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
