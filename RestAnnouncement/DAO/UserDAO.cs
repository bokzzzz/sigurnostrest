﻿using FootAppREST.Connection;
using MySql.Data.MySqlClient;
using RestAnnouncement.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAnnouncement.DAO
{
    public class UserDAO
    {
     
            public User GetUser(User user)
            {
                DatabaseConnection db = DatabaseConnection.GetConnection();
                MySqlCommand mySqlCommand = db.CreateCommand();
                mySqlCommand.CommandText = "select * from user where username=@username and password=@password and isActive=1";
                mySqlCommand.Parameters.AddWithValue("@username", user.Username);
                mySqlCommand.Parameters.AddWithValue("@password", user.Password);
                mySqlCommand.Prepare();
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    user.Role = reader.GetString("role");
                    user.Password = "";
                    reader.Close();
                return user;
                }
                reader.Close();
                return null;
            }
        public User[] GetUsers()
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            List<User> users = new List<User>();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "SELECT Username,Role,Changer FROM sigurnost.user where isActive=1; ";
            mySqlCommand.Prepare();
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                users.Add(new User
                {
                    Username = reader.GetString("Username"),
                    Role = reader.GetString("Role"),
                    Changer = reader.GetString("Changer")
                }); 
            }
            reader.Close();
            return users.ToArray() ;
        }
        public bool AddUser(User user)
        {
            if (!isUsernameExist(user.Username))
            {
                DatabaseConnection db = DatabaseConnection.GetConnection();
                MySqlCommand mySqlCommand = db.CreateCommand();
                mySqlCommand.CommandText = "INSERT into user(Username,Password,Role,Changer) values(@username,@password,@role,@changer);";
                mySqlCommand.Parameters.AddWithValue("@username",user.Username);
                mySqlCommand.Parameters.AddWithValue("@password",user.Password);
                mySqlCommand.Parameters.AddWithValue("@role",user.Role);
                mySqlCommand.Parameters.AddWithValue("@changer",user.Changer);
                mySqlCommand.Prepare();
                if (mySqlCommand.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                
            }
            return false;
        }
        private bool isUsernameExist(String username)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "SELECT count(username) as number from user where username=@username";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            if (reader.Read())
            {
                int number = reader.GetInt32("number");
                reader.Close();
                if (number == 0) return false;
                else return true;
            }
            reader.Close();
            return false;
        }
        public bool ChangeUser(User user)
        {
                DatabaseConnection db = DatabaseConnection.GetConnection();
                MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "UPDATE user set Password = @password ,Role = @role ,Changer = @changer where Username=@username;";
                mySqlCommand.Parameters.AddWithValue("@username", user.Username);
                mySqlCommand.Parameters.AddWithValue("@password", user.Password);
                mySqlCommand.Parameters.AddWithValue("@role", user.Role);
                mySqlCommand.Parameters.AddWithValue("@changer", user.Changer);
                mySqlCommand.Prepare();
                if (mySqlCommand.ExecuteNonQuery() > 0)
                {
                    return true;
                }

            
            return false;
        }
        public bool DeleteUser(User user)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "UPDATE user set isActive = 0  where Username=@username;";
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Prepare();
            if (mySqlCommand.ExecuteNonQuery() > 0)
            {
                return true;
            }


            return false;
        }
    }
    
}
