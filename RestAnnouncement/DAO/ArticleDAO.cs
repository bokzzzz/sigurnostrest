﻿using FootAppREST.Connection;
using MySql.Data.MySqlClient;
using RestUser.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestUser.DAO
{
    public class ArticleDAO
    {
        public Article[] GetArticles()
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            List<Article> articles = new List<Article>();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "SELECT * FROM Article where isActive=1; ";
            mySqlCommand.Prepare();
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                articles.Add(new Article
                {
                    Id=reader.GetInt32("IdArticle"),
                    Name=reader.GetString("Name"),
                    Price=reader.GetDecimal("Price"),
                    Quantity=reader.GetInt32("Quantity")
                });
            }
            reader.Close();
            return articles.ToArray();
        }
        public bool AddArticle(Article article)
        {
            if (!isArticleExist(article.Name))
            {
                DatabaseConnection db = DatabaseConnection.GetConnection();
                MySqlCommand mySqlCommand = db.CreateCommand();
                mySqlCommand.CommandText = "INSERT into article(Name,Quantity,Price) values(@name,@quantity,@price);";
                mySqlCommand.Parameters.AddWithValue("@name", article.Name);
                mySqlCommand.Parameters.AddWithValue("@quantity", article.Quantity);
                mySqlCommand.Parameters.AddWithValue("@price", article.Price);
                mySqlCommand.Prepare();
                if (mySqlCommand.ExecuteNonQuery() > 0)
                {
                    return true;
                }

            }
            return false;
        }
        private bool isArticleExist(String articleName)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "SELECT count(Name) as number from Article where Name=@name";
            mySqlCommand.Parameters.AddWithValue("@name", articleName);
            mySqlCommand.Prepare();
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            if (reader.Read())
            {
                int number = reader.GetInt32("number");
                reader.Close();
                if (number == 0) return false;
                else return true;
            }
            reader.Close();
            return false;
        }
        public bool DeleteArticle(int id)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "UPDATE article set isActive = 0  where IdArticle=@id;";
            mySqlCommand.Parameters.AddWithValue("@id",id);
            mySqlCommand.Prepare();
            if (mySqlCommand.ExecuteNonQuery() > 0)
            {
                return true;
            }


            return false;
        }
        public Article GetArticleById(int id)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            Article article = null;
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "SELECT * FROM Article where isActive=1 and IdArticle=@id; ";
            mySqlCommand.Parameters.AddWithValue("@id",id);
            mySqlCommand.Prepare();
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            if (reader.Read())
            {
                article=new Article
                {
                    Id = reader.GetInt32("IdArticle"),
                    Name = reader.GetString("Name"),
                    Price = reader.GetDecimal("Price"),
                    Quantity = reader.GetInt32("Quantity")
                };
            }
            reader.Close();
            return article;
        }
        public bool ChangeArticle(Article article)
        {
            if (!isArticleNameChange(article.Name,article.Id))
            {
                DatabaseConnection db = DatabaseConnection.GetConnection();
                MySqlCommand mySqlCommand = db.CreateCommand();
                mySqlCommand.CommandText = "UPDATE Article set Name = @name , Quantity = @quantity ,Price = @price where IdArticle=@id;";
                mySqlCommand.Parameters.AddWithValue("@name", article.Name);
                mySqlCommand.Parameters.AddWithValue("@quantity", article.Quantity);
                mySqlCommand.Parameters.AddWithValue("@price", article.Price);
                mySqlCommand.Parameters.AddWithValue("@id", article.Id);
                mySqlCommand.Prepare();
                if (mySqlCommand.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }

            return false;
        }
        private bool isArticleNameChange(String articleName,int id)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "SELECT count(Name) as number from Article where Name=@name and IdArticle != @id";
            mySqlCommand.Parameters.AddWithValue("@name", articleName);
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Prepare();
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            if (reader.Read())
            {
                int number = reader.GetInt32("number");
                reader.Close();
                if (number == 0) return false;
                else return true;
            }
            reader.Close();
            return true;
        }
    }
}
