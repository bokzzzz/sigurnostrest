﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestUser.DAO;
using RestUser.DTO;

namespace RestUser.Controllers
{
    [Route("security/bill")]
    public class BillController : Controller
    {
        [HttpPost("add")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public String AddBill([FromBody]Bill bill)
        {
            decimal totalPrice = 0.0m;
            foreach (Article article in bill.Articles)
                totalPrice += article.Price;
            bill.TotalPrice = totalPrice;
            bool isAdded=new BillDAO().AddBill(bill);
            return Convert.ToString(isAdded);
        }
        [HttpGet("get")]
        [Produces(MediaTypeNames.Application.Json)]
        public Bill GetBill()
        {
            Bill bill = new Bill();
            bill.Articles = new ArticleDAO().GetArticles();
            return bill;
        }
    }
}