﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestUser.DAO;
using RestUser.DTO;

namespace RestUser.Controllers
{
    [Route("security/article")]
    public class ArticleController : Controller
    {
        [HttpGet("articles")]
        [Produces(MediaTypeNames.Application.Json)]
        public Article[] GetItems()
        {
            return new ArticleDAO().GetArticles();
        }
        [HttpPost("add")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public String AddArticle([FromBody]Article article)
        {
            return Convert.ToString(new ArticleDAO().AddArticle(article));
        }
        [HttpPost("delete")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public String DeleteArticle([FromBody]int idArticle)
        {
            return Convert.ToString(new ArticleDAO().DeleteArticle(idArticle));
        }
        [HttpPost("change")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public String ChangeArticle([FromBody]Article article)
        {
            return Convert.ToString(new ArticleDAO().ChangeArticle(article));
        }
        [HttpPost("id")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public Article GetArticleById([FromBody]int id)
        {
            return new ArticleDAO().GetArticleById(id);
        }
    }
}