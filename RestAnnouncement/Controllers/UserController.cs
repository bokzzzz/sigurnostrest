﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestAnnouncement.DAO;
using RestAnnouncement.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RestAnnouncement.Controllers
{
    [Route("security")]
    public class UserController : Controller
    {
       
        [HttpPost("user")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public User GetLogin([FromBody]User user)
        {
            return new UserDAO().GetUser(user);
        }
        [HttpGet("users")]
        [Produces(MediaTypeNames.Application.Json)]
        public User[] GetUsers()
        {
            return new UserDAO().GetUsers();
        }
        [HttpPost("add")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public String AddUser([FromBody]User user)
        {
            return Convert.ToString(new UserDAO().AddUser(user));
        }
        [HttpPost("change")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public String ChangeUser([FromBody]User user)
        {
            return Convert.ToString(new UserDAO().ChangeUser(user));
        }
        [HttpPost("delete")]
        [Produces(MediaTypeNames.Application.Json)]
        [Consumes(MediaTypeNames.Application.Json)]
        public String DeleteUser([FromBody]User user)
        {
            return Convert.ToString(new UserDAO().DeleteUser(user));
        }
    }
}
