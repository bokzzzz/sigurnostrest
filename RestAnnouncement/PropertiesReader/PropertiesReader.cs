﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootAppREST
{
    class PropertiesReader
    {
        private Dictionary<string, string> Dict = new Dictionary<string, string>();

        public void Load(string filename)
        {
            string[] lines = File.ReadAllLines(filename);
            lines.ToList().ForEach(line =>
            {
            string[] info = null;
                if (line.Contains(" = "))
                    info = line.Split(new string[] { " = " }, StringSplitOptions.RemoveEmptyEntries);
                else if (line.Contains("="))
                    info = line.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                else
                    throw new InvalidPropertyFileException("Invalid Property File");
                if (info.Length != 2)
                    throw new InvalidPropertyFileException("Invalid Property File");
                string key = info[0];
                string value = info[1];
                Dict.Add(key, value);
            });
        }

        public string GetProperty(string propertyName)
        {   
            if (!Dict.TryGetValue(propertyName, out string value))
                throw new NoSuchPropertyException("Property " + propertyName + " doesn't exist");
            return value;
        }
    }
}
